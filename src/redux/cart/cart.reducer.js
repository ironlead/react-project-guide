import CartActionTypes from "./cart.types";

const INITIAL_STATE = {
  cartProducts: [],
};

const cartReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CartActionTypes.ADD_PRODUCT:
      return {
        ...state,
        cartProducts: state.cartProducts,
      };
    case CartActionTypes.REMOVE_PRODUCT:
      return {
        ...state,
        cartProducts: state.cartProducts,
      };
    default:
      return state;
  }
};

export default cartReducer;
